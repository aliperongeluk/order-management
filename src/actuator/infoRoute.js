const fs = require('fs');

function info(req, res) {
    const pkgjson = JSON.parse(fs.readFileSync('../../package.json', 'utf8'));

    packageName = pkgjson.name;
    packageDescription = pkgjson.description;
    packageVersion = pkgjson.version;

    res.status(200).json({
        build: {
            name: packageName,
            description: packageDescription,
            version: packageVersion
        }
    }).end();
};

module.exports = info;