function healthcheck(req, res) {
    res.status(200).json({ status: 'OK' }).end();
};

module.exports = healthcheck;