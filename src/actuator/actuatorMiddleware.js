const express = require('express');
const router = express.Router();
const infoRoute = require('./infoRoute');
const metricsRoute = require('./metricsRoute');
const healthcheckRoute = require('./healthcheckRoute');

function actuatorMiddleware(endpoint) {
    let infoPath = '/info';
    let metricsPath = '/metrics';
    let healthPath = '/health';

    if (endpoint) {
        infoPath = endpoint + infoPath;
        metricsPath = endpoint + metricsPath;
        healthPath = endpoint + healthPath;
    }

    router.get(infoPath, infoRoute);
    router.get(metricsPath, metricsRoute);
    router.get(healthPath, healthcheckRoute);

    return router;
 }

 module.exports = actuatorMiddleware;