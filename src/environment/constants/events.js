const events = {
  PAYMENT_COMPLETED: 'PAYMENT_COMPLETED',
  ORDER_CREATED: 'ORDER_CREATED',
  ORDER_PENDING: 'ORDER_PENDING',
  ORDER_PICKED: 'ORDER_PICKED',
  ORDER_SENT: 'ORDER_SENT',
  ORDER_RECEIVED: 'ORDER_RECEIVED',
  ORDER_RETURNED: 'ORDER_RETURNED',
  ORDER_CANCELLED: 'ORDER_CANCELLED',
  SHIPMENT_COMPLETED: 'SHIPMENT_COMPLETED',
};

module.exports = events;
