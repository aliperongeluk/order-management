const constants = {
  EXCHANGE_ORDER: 'order',
  EXCHANGE_SHIPMENT: 'shipment',
  EXCHANGE_PAYMENT: 'payment',
  SERVICE_NAME: 'order-management',
};

module.exports = constants;
