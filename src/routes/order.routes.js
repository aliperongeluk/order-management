const express = require('express');
const routes = express.Router();

const Order = require('../models/order.model');
const messageSender = require('../eventhandlers/message.sender');
const events = require('../environment/constants/events');

/**
 * @swagger
 * /api/order-management/orders:
 *    get:
 *     tags:
 *       - orders
 *     description: Retrieving all orders.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Orders are returned.
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', (req, res) => {
  Order.find({})
    .then(order => {
      res.status(200).json(order);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/order-management/orders/{id}:
 *    get:
 *     tags:
 *       - orders
 *     description: Retrieving order with given ID.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *         type: string
 *         required: true
 *         description: String ID of the order.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Orders are returned.
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Order.findById(id)
    .then(order => res.status(200).json(order))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/order-management/orders:
 *    post:
 *     tags:
 *       - orders
 *     description: Creating new order in order database. Also adds the newly created order to the message broker.
 *     parameters:
 *        - in: body
 *          name: body
 *          description: Create new order object.
 *          schema:
 *            required: true
 *            $ref: '#/definitions/Order'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Order is saved in database and returned.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/', (req, res) => {
  const order = new Order(req.body);

  order
    .save()
    .then(() => {
      messageSender.publish(events.ORDER_CREATED, order);
      res.status(200).send(order);
    })
    .catch(error => {
      console.log({ error: error });
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/order-management/orders/{id}:
 *    put:
 *      tags:
 *        - orders
 *      description: Update an order (soon only possible through authentication microservice)
 *      produces:
 *        - application/json
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: String ID of the order.
 *        - in: body
 *          name: body
 *          description: Update order attributes
 *          schema:
 *            type: object
 *            $ref: '#/definitions/Order'
 *      responses:
 *        200:
 *          description: Changes were successfully made.
 *        400:
 *          description: Failed to make changes to order.
 */
routes.put('/:id', (req, res) => {
  const id = req.params.id;

  Order.findByIdAndUpdate(id, req.body, { new: true, runValidators: true })
    .then(order => {
      switch (req.body.orderStatus) {
        case 'PENDING':
          messageSender.publish(events.ORDER_PENDING, order);
          break;
        case 'PICKED':
          messageSender.publish(events.ORDER_PICKED, order);
          break;
        case 'SENT':
          messageSender.publish(events.ORDER_SENT, order);
          break;
        case 'RECEIVED':
          messageSender.publish(events.ORDER_RECEIVED, order);
          break;
        case 'RETURNED':
          messageSender.publish(events.ORDER_RETURNED, order);
          break;
        case 'CANCELLED':
          messageSender.publish(events.ORDER_CANCELLED, order);
          break;
      }

      res.status(200).send(order);
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error(error);
      res.status(400).send(error);
    });
});

module.exports = routes;
