const express = require('express');
const routes = express.Router();

const Order = require('../models/order.model');
const messageSender = require('../eventhandlers/message.sender');
const events = require('../environment/constants/events');

routes.get('/', (req, res) => {
  const order = new Order({
    paymentStatus: 'NOT_PAID',
    orderStatus: 'PENDING',
    customerInformationId: '5ce663c7a67273001109d3eb',
    products: [
      {
        productId: '5ce3f3e51c0b1e0010bdfcb2',
        amount: 1,
      },
    ],
  });

  for (let i = 0; i < 10000; i++) {
    messageSender.publish(events.ORDER_CREATED, order);
  }

  res.status(200).send({ destroying: 'server' });
});

module.exports = routes;
