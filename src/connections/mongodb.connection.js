const mongoose = require('mongoose');
const config = require('../environment/config/mongodb.config');

mongoose.Promise = global.Promise;

mongoose.connect(config.dbUrl, { useNewUrlParser: true });
// mongoose.connect('mongodb://localhost/Order', {useNewUrlParser: true});
const connection = mongoose.connection
  .once('open', () => {
    console.log('Connected to Mongo on ' + config.dbUrl);
  })
  .on('error', error => {
    console.warn('Warning', error.toString());
  });

module.exports = connection;
