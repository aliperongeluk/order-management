// LOCAL: amqp://localhost
// SERVER: amqp://AliB:perongelukexpres@rabbitmq

const amqp = require('amqplib');
let counter = 0;

const constants = require('../environment/constants/constants');

const connect = () => {
  return new Promise((resolve, reject) => {
    amqp
      .connect('amqp://AliB:perongelukexpres@rabbitmq')
      .then(connection => {
        counter = 0;

        const createSendChannel = new Promise((resolve, reject) => {
          connection
            .createChannel()
            .then(sendChannel => {
              resolve(sendChannel);
            })
            .catch(error => {
              reject(error);
            });
        });

        const createReceiveChannel = new Promise((resolve, reject) => {
          connection
            .createChannel()
            .then(receiveChannel => {
              resolve(receiveChannel);
            })
            .catch(error => {
              reject(error);
            });
        });

        Promise.all([createReceiveChannel, createSendChannel]).then(
          channels => {
            channels.forEach(channel => {
              channel.assertExchange(constants.EXCHANGE_ORDER, 'topic', {
                durable: true,
              });
            });

            resolve({
              receiveChannel: channels[0],
              sendChannel: channels[1],
            });
          }
        );
      })
      .catch(error => {
        if (counter === 30) {
          reject(error);
        }

        setTimeout(() => {
          counter++;
          connect();
        }, 5000);
      });
  });
};

module.exports = { connect };
