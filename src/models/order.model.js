const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Order:
 *    type: object
 *    properties:
 *      _id:
 *        type: string
 *      paymentStatus:
 *        type: string
 *        enum: [NOT_PAID, PAID]
 *      orderStatus:
 *        type: string
 *        enum: [CREATED, PENDING, PICKED, SENT, RECEIVED, CANCELLED]
 *      shipmentId:
 *        type: string
 *      customerInformationId:
 *        type: string
 *      products:
 *        type: array
 *        items:
 *          type: object
 *          properties:
 *            productId:
 *              type: string
 *            amount:
 *              type: number
 */

const OrderSchema = new Schema(
  {
    paymentStatus: {
      type: Schema.Types.String,
      enum: ['NOT_PAID', 'PAID'],
      default: 'NOT_PAID',
    },
    orderStatus: {
      type: Schema.Types.String,
      enum: [
        'CREATED',
        'PENDING',
        'PICKED',
        'SENT',
        'RECEIVED',
        'CANCELLED',
        'RETURNED',
      ],
      required: [true, 'a status is required.'],
    },
    shipmentId: {
      type: Schema.Types.ObjectId,
    },
    customerInformationId: {
      type: Schema.Types.ObjectId,
      required: [true, 'a customer id is required.'],
    },
    products: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          required: [true, 'at least one productId is required.'],
        },
        amount: {
          type: Schema.Types.Number,
          required: [true, 'at least one of a product'],
          default: 1,
        },
      },
    ],
  },
  { timestamps: true }
);

const Order = mongoose.model('order', OrderSchema);

module.exports = Order;
