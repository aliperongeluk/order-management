let messageChannel;
const constants = require('../environment/constants/constants');

const publish = (topic, payload) => {
  messageChannel.publish(
    constants.EXCHANGE_ORDER,
    topic,
    new Buffer(JSON.stringify(payload)),
    {
      persistent: true,
    }
  );
};

const setMessageChannel = channel => {
  messageChannel = channel;
};

module.exports = { setMessageChannel, publish };
