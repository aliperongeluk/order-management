const constants = require('../../environment/constants/constants');
const events = require('../../environment/constants/events');
const messageSender = require('../message.sender');
const Order = require('../../models/order.model');

const consumeShipmentCompleted = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.SHIPMENT_COMPLETED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_SHIPMENT,
        events.SHIPMENT_COMPLETED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          try {
            const messageObject = JSON.parse(message.content.toString());

            const order = await Order.findById(messageObject.orderId);
            order.orderStatus = 'SENT';
            order.shipmentId = messageObject._id;

            await order.save();

            messageSender.publish(events.ORDER_SENT, order);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error);
          }
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeShipmentCompleted(messageChannel);
};

module.exports = { consume };
