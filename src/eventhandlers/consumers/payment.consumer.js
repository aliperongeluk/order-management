const constants = require('../../environment/constants/constants');
const events = require('../../environment/constants/events');

const Order = require('../../models/order.model');

const consumePaymentCompleted = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.PAYMENT_COMPLETED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_PAYMENT,
        events.PAYMENT_COMPLETED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          try {
            const messageObject = JSON.parse(message.content.toString());

            const order = await Order.findById(messageObject.orderId);
            order.paymentStatus = 'PAID';

            await order.save();
          } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error);
          }
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumePaymentCompleted(messageChannel);
};

module.exports = { consume };
