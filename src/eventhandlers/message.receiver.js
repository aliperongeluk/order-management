const constants = require('../environment/constants/constants');
const paymentConsumers = require('./consumers/payment.consumer');
const shipmentConsumers = require('./consumers/shipment.consumer');

let messageChannel;

const startConsuming = () => {
  createExchanges();

  paymentConsumers.consume(messageChannel);
  
  shipmentConsumers.consume(messageChannel);
};

const createExchanges = () => {
  messageChannel.assertExchange(constants.EXCHANGE_ORDER, 'topic', {
    durable: true,
  });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };
